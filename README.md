# CS5331
## Setup 
The tool was developped using `python3` but may work well with `python2`.
The following python packages need to be installed:  
- bs4  
- lxml  
- selenium  

They can be installed with `pip3 install bs4`.

## Usage  
To scan a single html file you can run:  

```python
python3 csp_generator.py path/to/html 0 http 'domain' 1
```
where where the second argument (0 here) specifies if we only want to only use the 
static analyzer (!=0) or not (0),
the third one (http) specifies the output format (http, cl, apache),
the forth is the domain of the webpage tested and the last one is
the verbose parameter.

To scan all the pages of a directory:  
- Update the script `analyzer/test_csp_generator.sh` and change the directory path  
- Update the `csp_generator.py` arguments if wanted  
- Run the script  

## Datasets 
### Web crawling 
To download the dataset, go to [https://commoncrawl.s3.amazonaws.com/crawl-data/CC-MAIN-2018-43/segments/1539583508988.18/warc/CC-MAIN-20181015080248-20181015101748-00000.warc.gz](https://commoncrawl.s3.amazonaws.com/crawl-data/CC-MAIN-2018-43/segments/1539583508988.18/warc/CC-MAIN-20181015080248-20181015101748-00000.warc.gz) (1.17Go).  
Unzip it to generate a warc archive (4.83Go) and extract web pages from Warc file. 

Many many websites crawled (46445 domains, around 4.7Go), my anti-virus spots some malicious ones and there are quite many with adult content...

To select only a subset of these pages, you can run the bash script: 

```bash
#!/bin/bash

mkdir dataset

ls CC-MAIN-20181015080248-20181015101748-00000 | sort -R | tail -100 |while read file;
do
  cp -r CC-MAIN-20181015080248-20181015101748-00000/$file dataset/
done

cd dataset

# Move all files to the parent directory
find . -mindepth 2 -type f -print -exec mv {} . \;
# remove sub-directories
find . -type d -empty -delete
# Add html extension to all files
find . -type f -exec mv '{}' '{}'.html \;
```

There is 3 datasets created with 20, 60 and 100 different web sites.  
__Be careful some have NSFW content, as they have not been filtered manually.__

### Vulnerable web applications
- NodeGoat ([github page](https://github.com/OWASP/NodeGoat))  

It is a vulnerable web application using Node.Js developed by OWASP. To install it, please follow the instructions on their git repository.  

The folder `nodegoat.zip` has been generated with the firefox plugin [Auto save HTML](https://github.com/nunoarruda/auto-save-html) by navigating through the application.  

The application has an XSS vulnerability in the "First name" field of the profile. We could show that with the right CSP the XSS can be prevented.

- There are lots of (intentionally) vulnerable applications, [here](https://www.owasp.org/index.php/OWASP_Vulnerable_Web_Applications_Directory_Project/Pages/Offline) is a list. 

## Python program
To learn more about the implementation please read the `report.pdf` file.  

## Browser extension
### To install the extension in Firefox

Enter `about:debugging` as an URL, click on "Load a temporary module" and select the `manifest.json` file.  

To test it you can:  

- Change the target pages in the `cspHeaders.js` file and set it to the URL to the page you want to test. (_There is some issues with some pages_)

- Run the `dummy_node_server`:  
  - Install node and npm   
  - Run `npm install` in the `dummy_node_server` directory  
  - Start the server with `node app.js`
