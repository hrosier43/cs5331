const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) =>
  res.send(
    '<!DOCTYPE html> <html lang="en"> <head> <meta charset="utf-8"> <title>Hello</title> </head> <body>Hello world!</body> </html>',
  ),
);

app.get('/cspHeaders', (req, res) => {
  res.setHeader('Content-Security-Provider', '*/*');
  res.send(
    '<!DOCTYPE html> <html lang="en"> <head> <meta charset="utf-8"> <title>Hello</title> </head> <body>Hello world!</body> </html>',
  );
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
