#!/bin/bash

ls CC-MAIN-20181015080248-20181015101748-00000 | sort -R | tail -20 |while read file;
do
  cp -r CC-MAIN-20181015080248-20181015101748-00000/$file dataset/
done

cd dataset
# Move all files to the parent directory
find . -mindepth 2 -type f -print -exec mv {} . \;
# remove sub-directories
find . -type d -empty -delete
# Add html extension to all files
find . -type f -exec mv '{}' '{}'.html \;

cd ..
