import os.path
ROOT_PATH = os.path.abspath(os.path.dirname(__file__)) + "/../"

# find this list on https://stackoverflow.com/questions/2725156/complete-list-of-html-tag-attributes-which-have-a-url-value
# depreceted attributes are not considered (ex: lowsrc, dynsrc, …)
MAPPING_TAG_URL_ATTRIBUTE = {
    # html4
    "a": "href",
    "applet": ["codebase", "archive"],
    "area": "href",
    "base": "href",
    "blockquote": "cite",
    "body": "background",
    "del": "cite",
    "form": "action",
    "frame": ["longdesc", "src"],
    "head": "profile",
    "iframe": ["longdesc", "src"],
    "img": ["longdesc", "src", "usemap", "srcset"],
    "input": ["src", "usemap"],
    "ins": "cite",
    "link": "href",
    "object": ["classid", "codebase", "data", "usemap", "archive"],
    "q": "cite",
    "script": "src",
    # html5
    "audio": "src",
    "button": "formaction",
    "command": "icon",
    "embed": "src",
    "html": "manifest",
    "input": ["formaction", "src"],
    "source": ["src", "srcset"],
    "track": "src",
    "video": ["poster", "src"],
    "meta": "content",
    "image": "href"
}

SPECIAL_ATTRIBUTES = ["srcset", "archive"]

TAGS = list(MAPPING_TAG_URL_ATTRIBUTE.keys())

# -1: deprecated; -2: experimental, -3: deleted, 1: ok
# types: document, font, image, media, other, script, stylesheet, xhr, ws
CSP_ATTRIBUTES = {
    "child-src": {"status": -1},  # use frame-src and worker-src
    "connect-src": {"status": 1},  # res loaded from script interface
    "default-src": {"status": 1},  # default value for any resource
    "font-src": {  # for CSS @font-face
        "status": 1,
        "type": "font",
    },
    "frame-src": {
        "status": 1,
        "tags": ["frame", "iframe"],
        "type": "document"
    },
    "img-src": {
        "status": 1,
        "tags": ["img", "input"],
        "type": "image",
        "special_tags": [{"tag": "link", "if_attribute": "icon"}]
    },
    "manifest-src": {
        "status": 1,
        "special_tags": [{"tag": "link", "if_attribute": "manifest"}]
    },
    "media-src": {
        "status": 1,
        "tags": ["audio", "video", "track"],
        "type": "media"
    },
    "object-src": {"status": 1, "tags": ["object", "embed", "applet"]},
    "prefetch-src": {"status": -2},  # not experimental but almost no info
    "script-src": {
        "status": 1,
        "tags": ["script"],
        "type": "script"
    },  # also for inline script event handlers and XSLT stylesheets
    "style-src": {
        "status": 1,
        "special_tags": [{"tag": "link", "if_attribute": "stylesheet"}],
        "type": "stylesheet"
    },
    "webrtc-src": {"status": -2},
    # Worker, SharedWorker, or ServiceWorker in JS scripts
    "worker-src": {"status": 1},
    "base-uri": {"status": 1, "tags": ["base"]},
    "plugin-types": {"status": 1, "tags": ["object", "embed", "applet"]},
    "sandbox": {"status": 1},
    "disown-opener": {"status": -2},
    "form-action": {
        "status": 1,
        "tags": ["form"]
    },
    "frame-ancestors": {"status": 1},
    "navigate-to": {"status": -2},
    "report-uri": {"status": -1},
    "report-to": {"status": -2},
    "block-all-mixed-content": {"status":  1},
    "referrer": {"status": -3},
    "require-sri-for": {"status": 1},
    "upgrade-insecure-requests": {"status": 1}
}

MAPPING_CSP_ATTRIBUTES_TAGS = [
    key for key, value in CSP_ATTRIBUTES.items() if "tags" in list(value.keys()) or "special_tags" in list(value.keys())]
MAPPING_CSP_ATTRIBUTES_SOURCE_TYPES = [
    key for key, value in CSP_ATTRIBUTES.items() if "type" in list(value.keys())]

# handler event list found  at https://docstore.mik.ua/orelly/webprog/jscript/ch19_01.htm

EVENTS_HANDLERS = ["onabort", "onblur", "onchange", "onclick", "ondblclick",
                   "onerror", "onfocus", "onkeydown", "onkeypress", "onkeyup", "onload",
                   "onmousedown", "onmousemove", "onmouseout", "onmouseover",
                   "onmouseup", "onreset", "onresize", "onselect", "onsubmit", "onunload"]

TAGS_WITH_HANDLERS = ["a", "area", "input", "select", "textarea", "form", "button",
                      "label", "select", "body", "frameset"]
