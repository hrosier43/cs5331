# -*- coding: utf-8 -*-
"""
CS5331 project
Extract all JS from an html file
"""

from lxml import html
import constant


def extract_inline_js(file_path):
    """return a tuple composed of a list with of all scripts find in the html 
    file and a list of all event handler which may have javascript embedded"""
    html_doc = html.parse(file_path).getroot()
    js_script_result = []
    js_event_result = []

    # Between a pair of <script> and </script> tags

    js_script = html_doc.cssselect("script")
    for i in js_script:
        js_script_result.append(i.text_content())

    # In an event handler https://docstore.mik.ua/orelly/webprog/jscript/ch12_02.htm
    for tag in constant.TAGS_WITH_HANDLERS:
        for event in constant.EVENTS_HANDLERS:
            elements = html_doc.cssselect(tag+"["+event+"]")
            for element in elements:
                if element != []:
                    res = {"tag": tag, "event": event,
                           "js": element.attrib[event]}
                    js_event_result.append(res)

    return(js_script_result, js_event_result)


def main():
    import sys
    if len(sys.argv) > 1:
        for i in range(1, len(sys.argv)):
            print(sys.argv[i], ":\n", extract_inline_js(sys.argv[i]), "\n")
    else:
        print(extract_inline_js(constant.ROOT_PATH +
                                "datasets/dataset20/pirate101-volcano-doubloon-80.html.html"))


if __name__ == '__main__':
    main()
