import re
from html_analyzer import analyze_html_file, extract_all_domains, generate_csp_header_associated_with_tags
from js_analyzer import generate_csp_header_from_js
from css_analyzer import generate_csp_header_from_css
from dynamic_resource_extractor import generate_csp_headers_from_dynamic_analyzer
import constant


def generate_csp_headers_from_static_analyzer(file_path, domain, verbose):
    html_info = analyze_html_file(file_path)
    csp_headers = generate_csp_header_associated_with_tags(html_info)

    csp_header_from_js = generate_csp_header_from_js(
        file_path, html_info, domain, verbose)
    for key in csp_header_from_js.keys():
        csp_headers[key] += csp_header_from_js[key]

    csp_header_from_css = generate_csp_header_from_css(
        file_path, html_info, domain, verbose)
    for key in csp_header_from_css.keys():
        if key in csp_headers:
            csp_headers[key] += csp_header_from_css[key]
        else:
            csp_headers[key] = csp_header_from_css[key]

    return csp_headers


def format_csp_cl(csp_header_dict):
    for csp_attribute, sources in csp_header_dict.items():
        if len(sources) == 0:
            print(csp_attribute, "'none'")
        else:
            print(csp_attribute, ' '.join(sources))


def format_csp_http(csp_header_dict):
    ret = ""
    for csp_attribute, sources in csp_header_dict.items():
        if len(sources) == 0:
            ret += csp_attribute + " 'none'; "
        else:
            ret += csp_attribute + " " + ' '.join(sources) + "; "
    print(ret)


def format_csp_apache(csp_header_dict):
    csp_attributes = ""
    for csp_attribute, sources in csp_header_dict.items():
        if len(sources) == 0:
            csp_attributes += csp_attribute + " 'none'; "
        else:
            csp_attributes += csp_attribute + ' ' + ' '.join(sources) + "; "

    print("<IfModule mod_headers.c>\n"
          "    Header unset Content-Security-Policy\n"
          "    Header set Content-Security-Policy \"" +
          csp_attributes[:-2] + ';”\n'
          "</IfModule>")


def compare_static_and_dynamic(csp_static, csp_dynamic):
    # common keys
    for key in set(csp_static.keys()).intersection(csp_dynamic.keys()):
        only_stat = set(csp_static[key])-set(csp_dynamic[key])
        if only_stat:
            print(key, "(in stat not in dyn): ", only_stat)
        only_dyn = set(csp_dynamic[key])-set(csp_static[key])
        if only_dyn:
            print(key, "(in dyn not in stat): ", only_dyn)

    # keys in static
    for key in set(csp_static.keys()) - set(csp_dynamic.keys()):
        if csp_static[key]:
            print(key, "(only stat): ", csp_static[key])

    # keys in dyn
    for key in set(csp_dynamic.keys()) - set(csp_static.keys()):
        if csp_dynamic[key]:
            print(key, "(only dyn): ", csp_dynamic[key])


def filter_url_if_domain_without_scheme(headers):
    for header in headers.keys():
        domains_without_scheme = [element for element in headers[header] if not (
            element.startswith("http") or element.startswith("'"))]
        for url in [url for url in headers[header] if url.startswith("http")]:
            domain = re.search(r'(?<=//)[^/]*', url, re.M).group()
            if domain in domains_without_scheme:
                headers[header].remove(url)

    return headers


def merge_static_and_dynamic_results(csp_static, csp_dynamic):
    headers = {}
    # common keys
    for key in set(csp_static.keys()).intersection(csp_dynamic.keys()):
        headers[key] = list(set(csp_static[key] + csp_dynamic[key]))

    # keys in static
    for key in set(csp_static.keys()) - set(csp_dynamic.keys()):
        headers[key] = csp_static[key]

    # keys in dyn
    for key in set(csp_dynamic.keys()) - set(csp_static.keys()):
        headers[key] = csp_dynamic[key]

    return headers


def generate_csp_headers(file_path, only_static=False, display='cl', domain=None, verbose=1):
    csp_static = generate_csp_headers_from_static_analyzer(
        file_path, domain, verbose)

    if verbose == 1:
        print("Static:")
        format_csp_cl(csp_static)

    if not only_static:
        csp_dynamic = generate_csp_headers_from_dynamic_analyzer(file_path)
        csp = merge_static_and_dynamic_results(csp_static, csp_dynamic)
        if verbose == 1:
            print("\nDynamic:")
            format_csp_cl(csp_dynamic)

            print("\nDiff:")
            compare_static_and_dynamic(csp_static, csp_dynamic)
    else:
        csp = csp_static

    csp = filter_url_if_domain_without_scheme(csp)

    print("\nCSP headers:")
    if display == 'cl':
        format_csp_cl(csp)
    elif display == 'Apache':
        format_csp_apache(csp)
    else:
        format_csp_http(csp)

    if verbose == 1:
        all_domains, a_tag_domains = extract_all_domains(file_path)

        # Don't consider domains from <a> tags
        domains_dict = {}
        for domain in all_domains:
            if not domain in domains_dict:
                domains_dict[domain] = all_domains.count(domain)
        for domain in a_tag_domains:
            domains_dict[domain] -= 1
        all_domains_not_from_a_tag = set([
            domain for domain in domains_dict.keys() if domains_dict[domain] > 0])

        domains_protected = set(
            [domain for _, domains in csp.items() for domain in domains])
        print("\nDomains found not in the headers and not in 'a' tags:\n",
              all_domains_not_from_a_tag-domains_protected)


def main():
    import sys
    if len(sys.argv) > 1:
        print(sys.argv[1], ":")
        generate_csp_headers(
            sys.argv[1], sys.argv[2] != '0', sys.argv[3], sys.argv[4], int(sys.argv[5]))
        print()
    else:
        file_path = constant.ROOT_PATH+"datasets/dataset20/06.html"
        generate_csp_headers(file_path)


if __name__ == '__main__':
    main()
