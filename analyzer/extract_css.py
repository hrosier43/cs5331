#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 13:33:30 2018

@author: isabellepere

CS5331 project
Extract all CSS from an html file

"""
from lxml import html
from bs4 import BeautifulSoup
import constant


def extract_inline_css(file_path):
    """return a tuple composed of the lists of internal and inlined css style found in the html file"""
    html_doc = html.parse(file_path).getroot()
    css_int_result = []
    css_inlined_result = []

    # Internal styles in style tags
    for element in html_doc.cssselect("style"):
        css_int_result.append(element.text_content())

    # Inlined style
    soup = BeautifulSoup(open(file_path, encoding='utf-8',
                              errors='ignore'), features="lxml")
    elements_with_style = soup.find_all(True, attrs={'style': True})
    for element in elements_with_style:
        css_inlined_result.append(element['style'])

    return(css_int_result, css_inlined_result)


def main():
    import sys
    if len(sys.argv) > 1:
        for i in range(1, len(sys.argv)):
            print(sys.argv[i], ":\n", extract_inline_css(sys.argv[i]), "\n")
    else:
        print(extract_inline_css(constant.ROOT_PATH +
                                 "datasets/dataset20/pirate101-volcano-doubloon-80.html.html"))


if __name__ == '__main__':
    main()
