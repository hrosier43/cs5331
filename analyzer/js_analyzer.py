import requests
import re
import constant
from extract_JS import extract_inline_js


def analyze_js(js_lines, verbose):
    js_info = {"js_eval": [], "css_eval": []}
    for line in js_lines:
        line = bytes(line, 'utf-8').decode('utf-8', 'ignore')
        # look for eval and related functions
        js_eval = re.findall(
            r'(eval|window\.setTimeout|window\.setInterval|window\.setImmediate|new Function) ?\( ?[\'\"]', line, re.M)
        if len(js_eval) > 0:
            js_info["js_eval"] += [(line, js_eval)]
            if verbose == 1:
                print(
                    "There may be a eval-like functions that are disabled by CSP.", (line, js_eval))

        # look for functions that create style declarations from strings
        # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/style-src#Unsafe_style_expressions
        css_eval = re.findall(
            r'(CSSStyleSheet\.insertRule|CSSGroupingRule\.insertRule|CSSStyleDeclaration\.cssText)', line, re.M)
        if len(css_eval) > 0:
            js_info["css_eval"] += [(line, css_eval)]
            if verbose == 1:
                print(
                    "There may be a css eval-like functions that are disabled by CSP.", (line, css_eval))

    return js_info


def analyze_js_file(file_path, verbose):
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src
    f = open(file_path, encoding='utf-8', errors='ignore')
    lines = f.readlines()
    f.close()
    return analyze_js(lines, verbose)


def generate_csp_header_from_js(html_file_path, html_info, domain, verbose):
    csp_headers = {"script-src": [], "style-src": []}
    any_js_eval_like_function = any_css_eval_like_function = False
    for js_uri in html_info["script"]:
        if js_uri.startswith("//"):
            js_uri = "http:" + js_uri
        elif domain:
            js_uri = "http://" + domain + "/" + js_uri
        else:
            continue
        if verbose == 1:
            print("Analyzing:", js_uri)
        r = requests.get(js_uri, verify=False)
        js_info = analyze_js(r.text.split('\n'), verbose)
        if len(js_info["js_eval"]) > 0:
            any_js_eval_like_function = True

        if len(js_info["css_eval"]) > 0:
            any_css_eval_like_function = True

    if any_js_eval_like_function:
        if verbose == 1:
            print(
                "There may be eval-like functions in the JS code that are disabled by the CSP policy")
            csp_headers["style-src"].append("'unsafe-eval'")
        # OR
        # answer = input(
        #     "It is not recommanded but do you want to disable this feature with 'unsafe-eval'?(y/n)")
        # if answer == 'y':
        #     csp_headers["style-src"].append("'unsafe-eval'")

    if any_css_eval_like_function:
        if verbose == 1:
            print("There may be functions in the JS code that are disabled by the CSP policy that create style declarations from strings")
            csp_headers["style-src"].append("'unsafe-eval'")
        # OR
        # answer = input(
        #     "It is not recommanded but do you want to disable this feature with 'unsafe-eval'?(y/n)")
        # if answer == 'y':
        #     csp_headers["script-src"].append("'unsafe-eval'")

    script_tag_js, event_scripts = extract_inline_js(html_file_path)
    if len(script_tag_js) != 0 or len(event_scripts) != 0:
        if verbose == 1:
            print("The web page", html_file_path, "has inline JS script:")
            script_js = '\n'.join(script_tag_js)
            print("In script tags:", script_js[:196]+" ..." if len(
                script_js) > 200 else script_js)
            if len(event_scripts) > 0:
                print("In event handler tags:",
                      event_scripts[:3] + ["..."] + event_scripts[-2:] if len(event_scripts) > 5 else event_scripts)

        csp_headers["script-src"].append("'unsafe-inline'")
        # OR
        # answer = input(
        #     "The CSP policy blocks the execution of this code. It is possible to disable the protection with 'unsafe-inline'.\nIt is not recommended but do you want to disable the protection?(y/n)")
        # if answer == 'y':
        #     csp_headers["script-src"].append("'unsafe-inline'")

    return csp_headers


def main():
    import sys
    if len(sys.argv) > 1:
        for i in range(1, len(sys.argv)):
            print(sys.argv[i], ":\n", analyze_js_file(sys.argv[i]), "\n", 1)
    else:
        print(analyze_js_file(constant.ROOT_PATH+"datasets/dataset20/06.html", 1))


if __name__ == '__main__':
    main()
