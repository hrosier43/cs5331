import requests
import re
import constant
from extract_css import extract_inline_css
from html_analyzer import get_domains_with_scheme


def analyze_css(css_lines, verbose):
    css_info = {"stylesheet": [], "image": [], "font": []}
    for line in css_lines:
        # look for calls to url after @import
        imports = re.findall(
            r'(?<=import\s)url\s*\(.*\)', line, re.M)
        if len(imports) > 0:
            # only consider http(s) resource
            for import_url in imports:
                stylesheets = re.findall(
                    r'(?<=\()[^\)]*(?=\))', import_url, re.M)
                css_info["stylesheet"] += stylesheets
                for stylesheet in stylesheets:
                    other_css_info = analyze_distant_stylesheet(
                        stylesheet, None, verbose)
                    for key in css_info.keys():
                        css_info[key] += other_css_info[key]

        # look for calls to url NOT after @import
        resources = re.findall(
            r'(?<!import\s)url\s*\(.*\)', line, re.M)
        if len(resources) > 0:
            # only consider http(s) resource
            for resource in resources:
                for url in re.findall(r'(?<=\()[^\)]*(?=\))', resource, re.M):
                    if re.search(r'\.(ttf|otf|woff|woff2)', url, re.M):
                        css_info["font"] += [url]
                    else:
                        css_info["image"] += [url]
    return css_info


def analyze_css_file(file_path, verbose=1):
    f = open(file_path, encoding='utf-8', errors='ignore')
    lines = f.readlines()
    f.close()
    return analyze_css(lines, verbose)


def analyze_distant_stylesheet(uri, domain, verbose):
    if verbose == 1:
        print("Analyzing:", uri)
    # load the resource
    if uri.startswith("//"):
        uri = "http:" + uri
    elif domain:
        uri = "http://" + domain + "/" + uri
    else:
        return {"stylesheet": [], "image": [], "font": []}
    r = requests.get(uri, verify=False)
    return analyze_css(r.text.split('\n'), verbose)


def add_headers_from_css_info(csp_headers, css_info):
    csp_headers["style-src"] += css_info["stylesheet"]
    csp_headers["font-src"] += css_info["font"]
    csp_headers["img-src"] += css_info["image"]


def generate_csp_header_from_css(html_file_path, html_info, domain, verbose=1):
    csp_headers = {"style-src": [], "img-src": [], "font-src": []}

    # look for urls in the stylesheet
    for uri in html_info["link"]["stylesheet"]:
        # look for urls in the css stylesheet
        css_info = analyze_distant_stylesheet(uri, domain, verbose)
        # keep only domains with scheme
        for key in css_info.keys():
            css_info[key], others = get_domains_with_scheme(css_info[key])
            # if others is not empty there may be local files
            if len(others) > 0:
                css_info[key].append("'self'")

        add_headers_from_css_info(csp_headers, css_info)

    style_tag_css, inline_css = extract_inline_css(html_file_path)
    if len(style_tag_css) != 0 or len(inline_css) != 0:
        if verbose == 1:
            print("The web page", html_file_path, "has inline CSS script:")
            style_css = '\n'.join(style_tag_css)
            print("In style tags: ", style_css[:196]+" ..." if len(
                style_css) > 200 else style_css)
            if len(inline_css) > 0:
                print("In event handler tags:",
                      inline_css[:3] + ["..."] + inline_css[-2:] if len(inline_css) > 5 else inline_css)

        csp_headers["style-src"].append("'unsafe-inline'")
        # OR
        # answer = input(
        #     "The CSP policy blocks the execution of this css. It is possible to disable the protection with 'unsafe-inline'.\nIt is not recommended but do you want to disable the protection?(y/n)")
        # if answer == 'y':
        #     csp_headers["style-src"].append("'unsafe-inline'")

        # look for urls in the inlines css
        css_info = analyze_css(style_tag_css + inline_css, verbose)
        # keep only domains with scheme
        for key in css_info.keys():
            css_info[key], _ = get_domains_with_scheme(css_info[key])
        add_headers_from_css_info(csp_headers, css_info)

    for key in csp_headers.keys():
        csp_headers[key] = list(set(csp_headers[key]))
    return csp_headers


def main():
    import sys
    if len(sys.argv) > 1:
        for i in range(1, len(sys.argv)):
            print(sys.argv[i], ":\n", analyze_css_file(sys.argv[i]), "\n")


if __name__ == '__main__':
    main()
