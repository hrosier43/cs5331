import json
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium import webdriver
from html_analyzer import get_domains_with_scheme
import constant


class DynamicResourceExtractor:
    def __init__(self):
        caps = DesiredCapabilities.CHROME
        caps['loggingPrefs'] = {'performance': 'ALL'}
        self.driver = webdriver.Chrome(desired_capabilities=caps)

    def getResourceInfo(self, url):
        ret = {}

        self.driver.get(url)
        for entry in self.driver.get_log('performance'):
            data = json.loads(entry["message"])
            if data["message"]["method"] == "Network.responseReceived":
                type_a = data["message"]["params"]["type"].lower()
                type_b = data["message"]["params"]["response"]["mimeType"].lower()
                url = data["message"]["params"]["response"]["url"]
                ret.setdefault(type_a, []).append((url, type_b))

                # print(type_a, type_b, url)

        return ret


def generate_csp_headers_from_dynamic_analyzer(file_path):
    extractor = DynamicResourceExtractor()
    extractor_info = extractor.getResourceInfo("file://"+file_path)
    csp_headers = {}
    for csp_attribute in constant.MAPPING_CSP_ATTRIBUTES_SOURCE_TYPES:
        domains = []
        type = constant.CSP_ATTRIBUTES[csp_attribute]['type']
        if type in extractor_info.keys():
            urls = [url[0] for url in extractor_info[type]]
            domains_with_scheme, others = get_domains_with_scheme(urls)
            domains += domains_with_scheme
        list.sort(domains)
        csp_headers[csp_attribute] = domains
    return csp_headers


if __name__ == '__main__':
    extractor = DynamicResourceExtractor()

    # example for online
    extractor.getResourceInfo('http://www.bbc.com/')

    # example for local
    import os
    print(extractor.getResourceInfo("file://" + constant.ROOT_PATH +
                                    "datasets/dataset20/pirate101-volcano-doubloon-80.html.html"))
