from lxml import html
import re
import constant


def filter_uri(elements):
    # remove empty strings and duplicates
    elements = list(filter(None, list(set(elements))))
    elements = list(
        filter(lambda uri: re.search(r'/|\.', uri, re.I), elements))
    return elements


def get_domains_with_scheme(URIs):
    domains_with_scheme, others = set(), set()
    for url in URIs:
        if url.startswith('//'):
            domains_with_scheme.add(
                re.search(r'(?<=//)[^/]*', url, re.M | re.I).group())
        else:
            domain_with_scheme = re.search(r'https?://[^/]*', url, re.M | re.I)
            if domain_with_scheme:
                domains_with_scheme.add(domain_with_scheme.group())
            else:
                others.add(url)
    return list(domains_with_scheme), list(others)


# meta tag more complex: https://gist.github.com/kevinSuttle/1997924
def handle_meta_tag(html_doc):
    URIs = []
    for element in html_doc.cssselect('meta'):
        attributes = list(element.attrib.keys())
        attribute = constant.MAPPING_TAG_URL_ATTRIBUTE['meta']
        # if the meta tag has a content
        if attribute in attributes:
            if "http-equiv" in attributes:
                url = re.search(
                    r'(?<=url=)[^; ]*', element.attrib[attribute], re.M | re.I)
                if url:
                    URIs += [url.group()]
    return URIs


def handle_link_tag(html_doc):
    URIs = {"stylesheet": [], "manifest": [], "icon": []}
    for element in html_doc.cssselect('link'):
        attributes = list(element.attrib.keys())
        attribute = constant.MAPPING_TAG_URL_ATTRIBUTE['link']
        rel = element.attrib["rel"]
        url = element.attrib[attribute]
        if "stylesheet" == rel:
            URIs["stylesheet"].append(url)
        elif "manifest" == rel:
            URIs["manifest"].append(url)
        elif re.search(r'icon', rel, re.I | re.M):
            URIs["icon"].append(url)
    URIs["stylesheet"] = filter_uri(URIs["stylesheet"])
    URIs["manifest"] = filter_uri(URIs["manifest"])
    return URIs


def add_attribute_value_if_exist(URIs, element, attribute):
    if attribute in list(element.attrib.keys()):
        if attribute in constant.SPECIAL_ATTRIBUTES:
            URIs += re.split(',| ', element.attrib[attribute])
        else:
            URIs += [element.attrib[attribute]]
    return URIs


def get_uris_of_tag(html_doc, tag):
    if tag == "meta":
        URIs = handle_meta_tag(html_doc)
    elif tag == "link":
        return handle_link_tag(html_doc)
    else:
        URIs = []
        for element in html_doc.cssselect(tag):
            if type(constant.MAPPING_TAG_URL_ATTRIBUTE[tag]) == list:
                for attribute in constant.MAPPING_TAG_URL_ATTRIBUTE[tag]:
                    URIs = add_attribute_value_if_exist(
                        URIs, element, attribute)
            else:
                attribute = constant.MAPPING_TAG_URL_ATTRIBUTE[tag]
                URIs = add_attribute_value_if_exist(URIs, element, attribute)

    return filter_uri(URIs)


def analyze_html_file(file_path):
    html_doc = html.parse(file_path).getroot()
    URIs_by_tag = {}
    for tag in constant.TAGS:
        URIs_by_tag[tag] = get_uris_of_tag(html_doc, tag)
    return URIs_by_tag


def extract_all_domains(file_path):
    f = open(file_path, encoding='utf-8', errors='ignore')
    lines = f.readlines()
    f.close()
    domains = []
    for line in lines:
        line = bytes(line, 'utf-8').decode('utf-8', 'ignore')
        domains += re.findall(r'(?<=//)[^/ ,\'\"\?]*', line, re.I | re.M)

    # get the domains from <a> tags
    html_doc = html.parse(file_path).getroot()
    a_tag_domains = []
    for element in html_doc.cssselect('a'):
        a_tag_domains = add_attribute_value_if_exist(
            a_tag_domains, element, 'href')
    a_tag_domains = list(filter(lambda uri: uri.startswith(
        "http") or uri.startswith("//"), a_tag_domains))
    a_tag_domains = map(lambda uri: re.search(
        r'(?<=//)[^/ ,\'\"\?]*', uri, re.I | re.M).group(), a_tag_domains)
    return domains, a_tag_domains


def generate_csp_header_associated_with_tags(html_info):
    csp_headers = {}
    for csp_attribute in constant.MAPPING_CSP_ATTRIBUTES_TAGS:
        domains = []
        if "tags" in constant.CSP_ATTRIBUTES[csp_attribute].keys():
            tags_concerned = constant.CSP_ATTRIBUTES[csp_attribute]['tags']
            for html_tag in tags_concerned:
                domains_with_scheme, others = get_domains_with_scheme(
                    html_info[html_tag])
                domains += domains_with_scheme
                for other in others:
                    # relative linked file
                    domains += ["'self'"]
                    # display info if not sure if it is a file
                    if not re.search(r'/', other, re.M) and len(others) == 1:
                        print("!! Don't add 'self' to ", csp_attribute,
                              "if this", other, "is not a file")
        if "special_tags" in constant.CSP_ATTRIBUTES[csp_attribute].keys():
            for special_tag in constant.CSP_ATTRIBUTES[csp_attribute]['special_tags']:
                domains_with_scheme, others = get_domains_with_scheme(
                    html_info[special_tag["tag"]][special_tag["if_attribute"]])
                domains += domains_with_scheme
                for other in others:
                    # relative linked file
                    domains += ["'self'"]
                    # display info if not sure if it is a file
                    if not re.search(r'/', other, re.M) and len(others) == 1:
                        print("!! Don't add 'self' to ", csp_attribute,
                              "if this", other, "is not a file")

        # remove duplicated 'self'
        domains = list(set(domains))
        # not useful for headers but help readability
        list.sort(domains)
        csp_headers[csp_attribute] = domains

    return csp_headers


def main():
    import sys
    if len(sys.argv) > 1:
        for i in range(1, len(sys.argv)):
            print(sys.argv[i], ":\n", analyze_html_file(sys.argv[i]), "\n")
    else:
        print(analyze_html_file(constant.ROOT_PATH+"datasets/dataset20/06.html"))


if __name__ == '__main__':
    main()
