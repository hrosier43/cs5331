function displayCSPHeaders(e) {
  let filter = browser.webRequest.filterResponseData(e.requestId);
  let decoder = new TextDecoder('utf-8');
  let encoder = new TextEncoder();
  var cspHeaders = '';
  e.responseHeaders.forEach(header => {
    if (header.name.toLowerCase() == 'content-security-provider')
      cspHeaders = header.value;
  });

  filter.ondata = async event => {
    let str = decoder.decode(event.data, {stream: false});

    let el = document.createElement('html');
    el.innerHTML = str;
    let body = el.getElementsByTagName('body')[0];
    let style, content;
    if (cspHeaders) {
      style = 'style="text-align: right;background-color: green;"';
      content = 'Content-security-provider headers: ' + cspHeaders;
    } else {
      style = 'style="text-align: right;background-color: red;"';
      content = 'No CSP header';
    }
    body.innerHTML =
      '<div ' + style + '>' + content + '</div>' + body.innerHTML;

    filter.write(encoder.encode(el.innerHTML));
    filter.disconnect();
    return str;
  };
}

var targetPage = 'http://localhost/*';
// Listen for onHeaderReceived for the target page.
browser.webRequest.onHeadersReceived.addListener(
  displayCSPHeaders,
  {urls: [targetPage]},
  ['blocking', 'responseHeaders'],
);
